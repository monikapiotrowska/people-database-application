package pl.sda.dao;

import pl.sda.domain.Person;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Optional;


public class PersonDaoJPAImpl implements PersonDAO {

    private static final EntityManagerFactory emf;

    static {
        emf = Persistence.createEntityManagerFactory("jpa-dao");
    }

    @Override
    public List<Person> basicPeopleInfo() {
        return null;
    }

    @Override
    public Optional<Person> findById(int id) {
        return Optional.empty();
    }

    @Override
    public int create(Person person) {
        return 0;
    }

    @Override
    public int update(Person person) {
        return 0;
    }

    @Override
    public int updateAddress(Person person) {
        return 0;
    }

    @Override
    public void delete(int id) {
    }
}
