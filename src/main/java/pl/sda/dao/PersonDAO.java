package pl.sda.dao;

import pl.sda.domain.Person;

import java.util.List;
import java.util.Optional;

public interface PersonDAO {

    List<Person> basicPeopleInfo();

    Optional<Person> findById(int id);

    int create(Person person);

    int update(Person person);

    int updateAddress(Person person);

    void delete(int id);
}
