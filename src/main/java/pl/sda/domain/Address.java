package pl.sda.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
@Table(name = "address")
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "city")
    private String city;

    @Column(name = "postCode")
    private String postCode;

    @Column(name = "street")
    private String street;

    @Column(name = "homeNumber")
    private int homeNumber;

    @Column(name = "apNumber")
    private int apNumber;
}
